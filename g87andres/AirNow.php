<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

namespace g87andres;

use g87andres\AirNow\AbstractCache;
use g87andres\AirNow\Exception as AirNowException;
use g87andres\AirNow\Fetcher\CurlFetcher;
use g87andres\AirNow\Fetcher\FetcherInterface;
use g87andres\AirNow\Fetcher\FileGetContentsFetcher;
use g87andres\AirNow\Forecast;

/**
 * Main class for the AirNow-PHP-API. Only use this class.
 *
 * @api
 */
class AirNow
{
    /**
     * @var string $aqForecastZipUrl The basic api url to fetch air quality data for a zip code.
     */
    private $forecastUrl = "http://www.airnowapi.org/aq/forecast/%s/?";

    /**
     * @var \g87andres\AirNow\AbstractCache|bool $cacheClass The cache class.
     */
    private $cacheClass = false;

    /**
     * @var int
     */
    private $seconds;

    /**
     * @var FetcherInterface The url fetcher.
     */
    private $fetcher;

    /**
     * Constructs the AirNow object.
     *
     * @param null|FetcherInterface $fetcher    The interface to fetch the data from AirNow. Defaults to
     *                                          CurlFetcher() if cURL is available. Otherwise defaults to
     *                                          FileGetContentsFetcher() using 'file_get_contents()'.
     * @param bool|string           $cacheClass If set to false, caching is disabled. Otherwise this must be a class
     *                                          extending AbstractCache. Defaults to false.
     * @param int                   $seconds    How long air quality data shall be cached. Default 10 minutes.
     *
     * @throws \Exception If $cache is neither false nor a valid callable extending g87andres\AirNow\AbstractClass.
     * @api
     */
    public function __construct($fetcher = null, $cacheClass = false, $seconds = 600)
    {
        if ($cacheClass !== false && !($cacheClass instanceof AbstractCache)) {
            throw new \Exception("The cache class must implement the FetcherInterface!");
        }
        if (!is_numeric($seconds)) {
            throw new \Exception("\$seconds must be numeric.");
        }
        if (!isset($fetcher)) {
            $fetcher = (function_exists('curl_version')) ? new CurlFetcher() : new FileGetContentsFetcher();
        }
        if ($seconds == 0) {
            $cacheClass = false;
        }

        $this->cacheClass = $cacheClass;
        $this->seconds = $seconds;
        $this->fetcher = $fetcher;
    }

    /**
     * Returns the current air quality at the place you specified as an object.
     *
     * @param array|int|string  $query      The place to get air quality date for.  For possible values see below.
     * @param string            $type       Possible values are zipCode or latLong
     * @param null              $date       Date in YYYY-MM-DD format.
     * @param int               $distance   Radius distance in miles to gather data for.
     * @param string            $appid      Your API Key.  See https://docs.airnowapi.org/ for more details.
     * @return Forecast
     * @throws AirNowException If AirNow returns an error.
     *
     * There are two days to specify the place to get air quality information for:
     * - Use the 5-digit zip code.
     * - Use the coordinates: $query must be an associative array containing the 'lat' and 'lon' values.
     *
     * @api
     */
    public function getForecast($query, $type = 'zipCode', $date = null, $distance = 25, $appid = '')
    {
        // Disable default error handling of SimpleXML (Do not throw E_WARNINGs).
        libxml_use_internal_errors(true);
        libxml_clear_errors();

        $answer = $this->getRawForecastData($query, $type, $date, $distance, $appid, 'application/xml');

        try {
            $xml = new \SimpleXMLElement($answer);
        } catch (\Exception $e) {
            // Invalid xml format. This happens in case AirNow returns an error.
            // AirNow always uses json for errors, even if one specifies xml as format.
            $error = json_decode($answer, true);
            if (isset($error['message'])) {
                throw new AirNowException($error['message'], $error['cod']);
            } else {
                throw new AirNowException('Unknown fatal error: AirNow returned the following json object: ' . $answer);
            }
        }

        return new Forecast($xml, $type);
    }

    /**
     * Returns the xml/json string returned by AirNow for current air quality.
     *
     * @param array|int|string  $query      The place to get air quality date for.  For possible values see below.
     * @param string            $type       Possible values are zipCode or latLong
     * @param null              $date       Date in YYYY-MM-DD format.
     * @param int               $distance   Radius distance in miles to gather data for.
     * @param string            $appid      Your API Key.  See https://docs.airnowapi.org/ for more details.
     * @param string            $mode       Possible values: application/xml , application/json
     * @return string
     *
     * There are two days to specify the place to get air quality information for:
     * - Use the 5-digit zip code.
     * - Use the coordinates: $query must be an associative array containing the 'lat' and 'lon' values.
     *
     * @api
     */
    public function getRawForecastData($query, $type = 'zipCode', $date = null, $distance = 25, $appid = '', $mode = 'application/xml')
    {
        $url = $this->buildUrl($query, $type, $date, $distance, $appid, $mode, $this->forecastUrl);

        return $this->cacheOrFetchResult($url);
    }

    /**
     * Fetches the result or delivers a cached version of the result.
     *
     * @param $url
     *
     * @return string
     *
     * @internal
     */
    private function cacheOrFetchResult($url)
    {
        if ($this->cacheClass !== false) {
            /** @var \g87andres\AirNow\AbstractCache $cache */
            $cache = $this->cacheClass;
            $cache->setSeconds($this->seconds);
            if ($cache->isCached($url)) {
                return $cache->getCached($url);
            }
            $result = $this->fetcher->fetch($url);
            $cache->setCached($url, $result);
        } else {
            $result = $this->fetcher->fetch($url);
        }

        return $result;
    }

    /**
     * Build the url to fetch air quality data from.
     *
     * @param        $query
     * @param        $units
     * @param        $lang
     * @param        $appid
     * @param        $mode
     * @param string $url The url to prepend.
     *
     * @return bool|string The fetched url, false on failure.
     *
     * @internal
     */
    private function buildUrl($query, $type, $date, $distance, $appid, $mode, $url)
    {
        if($type != "zipCode" && $type != "latLong")
            throw new \InvalidArgumentException('Error: $type is not valid.  Valid types are zipCode or latLong.');

        if($date == null)
            $date = date('Y-m-d', time());

        $queryUrl = $this->buildQueryUrlParameter($query, $type);

        $url = str_replace("%s", $type, $url) . "format={$mode}&{$queryUrl}&date={$date}&distance={$distance}";
        if (!empty($appid)) {
            $url .= "&API_KEY={$appid}";
        }

        return $url;
    }

    /**
     * Builds the query string for the url.
     *
     * @param $query
     *
     * @return string The built query string for the url.
     * @throws \InvalidArgumentException If the query parameter is invalid.
     *
     * @internal
     */
    private function buildQueryUrlParameter($query, $type)
    {
        switch($type)
        {
            default:
            case "zipCode":
                if(is_numeric($query))
                    return "zipCode={$query}";
                else
                    throw new \InvalidArgumentException('Error: $query has the wrong format. Must be 5-digit zipcode.');
            break;
            case "latLong":
                if (is_array($query) && isset($query['lat']) && isset($query['lon']) && is_numeric($query['lat']) && is_numeric($query['lon']))
                    return "latitude={$query['lat']}&longitude={$query['lon']}";
                else
                    throw new \InvalidArgumentException("Error: $query has the wrong format.  Must be in array('lat' => xxx, 'lon' => xxx) format.");
                break;
        }
    }
}
