<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

namespace g87andres\AirNow\Fetcher;

/**
 * Interface FetcherInterface.
 *
 * @api
 */
interface FetcherInterface
{
    /**
     * Fetch contents from the specified url.
     *
     * @param string $url The url to be fetched.
     *
     * @return string The fetched content.
     *
     * @api
     */
    public function fetch($url);
}
