<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

namespace g87andres\AirNow;

use g87andres\AirNow;

/**
 * Air Quality class used to hold the forecast data.
 */
class Forecast
{

    /**
     *Date the forecast was issued.
     *
     * @var \DateTime
     */
    public $dateIssue;

    /**
     * Date for which the forecast applies.
     *
     * @var \DateTime
     */
    public $dateForecast;

    /**
     * City of area name for which the forecast applies.
     *
     * @var \SimpleXMLElement[]
     */
    public $reportingArea;

    /**
     * Two-character state abbreviation.
     *
     * @var \SimpleXMLElement[]
     */
    public $stateCode;

    /**
     * Latitude in decimal degrees.
     *
     * @var \SimpleXMLElement[]
     */
    public $latitude;

    /**
     * Longitude in decimal degrees.
     *
     * @var \SimpleXMLElement[]
     */
    public $longitude;

    /**
     * Forecasted parameter name.
     *
     * @var \SimpleXMLElement[]
     */
    public $parameterName;

    /**
     * Numerical AQI value forecasted. When a numerical AQI value is not available, such as when only a categorical forecast has been submitted, a -1 will be returned.
     *
     * @var \SimpleXMLElement[]
     */
    public $aqi;

    /**
     * Color string assigned to each AQI value range.
     *
     * @var string
     */
    public $aqiColor;

    /**
     * Forecasted AQI category number:
     * 1. Good
     * 2. Moderate
     * 3. Unhealthy for Sensitive Groups
     * 4. Unhealthy
     * 5. Very Unhealthy
     * 6. Hazardous
     * 7. Unavailable
     *
     * @var \SimpleXMLElement[]
     */
    public $categoryNumber;

    /**
     * Name of the AQI category:
     * 1. Good
     * 2. Moderate
     * 3. Unhealthy for Sensitive Groups
     * 4. Unhealthy
     * 5. Very Unhealthy
     * 6. Hazardous
     * 7. Unavailable
     *
     * @var \SimpleXMLElement[]
     */
    public $categoryName;


    /**
     * Create a new forecast object.
     *
     * @param \SimpleXMLElement $xml
     * @param string            $units
     *
     * @internal
     */
    public function __construct(\SimpleXMLElement $xml, $type='zipCode')
    {
        if($type == 'zipCode')
            $data = $xml->ForecastByZip;
        else
            $data = $xml->ForecastByLatLon;

        // Cycle through each iteration to find ParameterName = PM2.5
        foreach($data as $k => $v)
        {
            if($v->ParameterName == 'PM2.5')
            {
                $this->dateIssue = new \DateTime($v->DateIssue, new \DateTimeZone('UTC'));
                $this->dateForecast = new \DateTime($v->DateForecast, new \DateTimeZone('UTC'));
                $this->reportingArea = $v->ReportingArea;
                $this->stateCode = $v->StateCode;
                $this->latitude = $v->Latitude;
                $this->longitude = $v->Longitude;
                $this->parameterName = $v->ParameterName;
                $this->aqi = $v->AQI;
                $this->aqiColor = $this->aqi_color($this->aqi);
                $this->categoryNumber = $v->CategoryNumber;
                $this->categoryName = $v->CategoryName;
            }
        }
    }

    private function aqi_color($aqi)
    {
        if($aqi >= 0 && $aqi <= 50)
            return 'green';
        elseif($aqi >= 51 && $aqi <= 100)
            return 'yellow';
        elseif($aqi >= 101 && $aqi <= 150)
            return 'orange';
        elseif($aqi >= 151 && $aqi <= 200)
            return 'red';
        elseif($aqi >= 201 && $aqi <= 300)
            return 'purple';
        elseif($aqi >= 301 && $aqi <= 500)
            return 'maroon';
        else
            return 'black';
    }
}
