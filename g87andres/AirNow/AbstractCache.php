<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

namespace g87andres\AirNow;

/**
 * Abstract cache class to be overwritten by custom cache implementations.
 */
abstract class AbstractCache
{
    /**
     * @var int $seconds Cache time in seconds.
     */
    protected $seconds;

    /**
     * Checks whether a cached air quality data is available.
     *
     * @param string $url The unique url of the cached content.
     *
     * @return bool False if no cached information is available, otherwise true.
     *
     * You need to check if a cached result is outdated here. Return false in that case.
     */
    abstract public function isCached($url);

    /**
     * Returns cached air quality data.
     *
     * @param string $url The unique url of the cached content.
     *
     * @return string|bool The cached data if it exists, false otherwise.
     */
    abstract public function getCached($url);

    /**
     * Saves cached air quality data.
     *
     * @param string $url     The unique url of the cached content.
     * @param string $content The air quality data to cache.
     *
     * @return bool True on success, false on failure.
     */
    abstract public function setCached($url, $content);

    /**
     * Set after how much seconds the cache shall expire.
     *
     * @param int $seconds
     */
    public function setSeconds($seconds)
    {
        $this->seconds = $seconds;
    }
}
