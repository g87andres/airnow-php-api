<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

namespace g87andres\AirNow\Tests\Fetcher;

use \g87andres\AirNow\Fetcher\CurlFetcher;

/**
 * @requires function curl_version
 */
class CurlFetcherTest extends \PHPUnit_Framework_TestCase
{
    public function testInvalidUrl()
    {
        $fetcher = new CurlFetcher();

        $content = $fetcher->fetch('http://notexisting.example.com');

        $this->assertSame(false, $content);
    }

    public function testEmptyUrl()
    {
        $fetcher = new CurlFetcher();

        $content = $fetcher->fetch('');

        $this->assertSame(false, $content);
    }

    public function testValidUrl()
    {
        $fetcher = new CurlFetcher();

        $content = $fetcher->fetch('http://httpbin.org/html');

        $this->assertContains('Herman Melville', $content);
    }
}
