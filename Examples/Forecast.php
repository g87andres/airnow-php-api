<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

use g87andres\AirNow;
use g87andres\AirNow\Exception as AirNowException;

if (file_exists('../vendor/autoload.php')) {
    // Library is not part of a project. "composer install" was executed directly on this library's composer file.
    require('../vendor/autoload.php');
} else {
    // Library is part of a project.
    /** @noinspection PhpIncludeInspection */
    require('../../../autoload.php');
}


// Get AirNow object. This object is not using caching.
$airnow = new AirNow();

// Your API Key
$appid = '';

$forecast = $airnow->getForecast(92782, 'zipCode', null, null, $appid);

echo "Date Issue: " . $forecast->dateIssue->format('r');
echo "<br />\n";

echo "Date Forecast: " . $forecast->dateForecast->format('r');
echo "<br />\n";

echo "Reporting Area: " . $forecast->reportingArea;
echo "<br />\n";

echo "State Code: " . $forecast->stateCode;
echo "<br />\n";

echo "Latitude: " . $forecast->latitude;
echo "<br />\n";

echo "Longitude: " . $forecast->longitude;
echo "<br />\n";

echo "Parameter Name: " . $forecast->parameterName;
echo "<br />\n";

echo "AQI Value: " . $forecast->aqi;
echo "<br />\n";

echo "AQI Color: " . $forecast->aqiColor;
echo "<br />\n";

echo "Category Name: " . $forecast->categoryName;
echo "<br />\n";

echo "Category Number: " . $forecast->categoryNumber;
echo "<br />\n";
