<?php
/**
 * AirNow-PHP-API — A php api to parse air quality data from http://www.airnow.gov .
 *
 * @license MIT
 *
 * Please see the LICENSE file distributed with this source code for further
 * information regarding copyright and licensing.
 *
 * Please visit the following links to read about the usage policies and the license of
 * AirNow before using this class:
 *
 * @see http://www.airnow.gov
 */

use g87andres\AirNow;
use g87andres\AirNow\AbstractCache;

if (file_exists('../vendor/autoload.php')) {
    // Library is not part of a project. "composer install" was executed directly on this library's composer file.
    require('../vendor/autoload.php');
} else {
    // Library is part of a project.
    /** @noinspection PhpIncludeInspection */
    require('../../../autoload.php');
}

/**
 * Example cache implementation.
 *
 * @ignore
 */
class ExampleCache extends AbstractCache
{
    private function urlToPath($url)
    {
        $tmp = sys_get_temp_dir();
        $dir = $tmp . DIRECTORY_SEPARATOR . "AirNowPHPApi";
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $path = $dir . DIRECTORY_SEPARATOR . md5($url);

        return $path;
    }

    /**
     * @inheritdoc
     */
    public function isCached($url)
    {
        $path = $this->urlToPath($url);
        if (!file_exists($path) || filectime($path) + $this->seconds < time()) {
            echo "Air Quality data is NOT cached!\n";

            return false;
        }

        echo "Air Quality data is cached!\n";

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getCached($url)
    {
        return file_get_contents($this->urlToPath($url));
    }

    /**
     * @inheritdoc
     */
    public function setCached($url, $content)
    {
        file_put_contents($this->urlToPath($url), $content);
    }
}

// Get AirNow object. This object is not using caching.
$airnow = new AirNow(null, new ExampleCache(), 10);

// Your API Key
$appid = '';

$forecast = $airnow->getForecast(92782, 'zipCode', null, null, $appid);

echo "<br /><br />\n\n";

echo "Date Issue: " . $forecast->dateIssue->format('r');
echo "<br />\n";

echo "Date Forecast: " . $forecast->dateForecast->format('r');
echo "<br />\n";

echo "Reporting Area: " . $forecast->reportingArea;
echo "<br />\n";

echo "State Code: " . $forecast->stateCode;
echo "<br />\n";

echo "Latitude: " . $forecast->latitude;
echo "<br />\n";

echo "Longitude: " . $forecast->longitude;
echo "<br />\n";

echo "Parameter Name: " . $forecast->parameterName;
echo "<br />\n";

echo "AQI Value: " . $forecast->aqi;
echo "<br />\n";

echo "AQI Color: " . $forecast->aqiColor;
echo "<br />\n";

echo "Category Name: " . $forecast->categoryName;
echo "<br />\n";

echo "Category Number: " . $forecast->categoryNumber;
echo "<br />\n";
