AirNow-PHP-Api
======================
A php api to parse air quality data from [airnow.gov](http://www.airnow.gov). This api tries to normalise and abstract the data and remove inconsistencies.

-----------

For example code and how to use this api, please take a look into `Examples_*.php` files and run them in your browser.
- `Examples_Forecast.php` Shows how to receive air quality forecasts.
- `Examples_Cache.php` Shows how to implement a cache.

**Notice:** This api is not made by AirNow, nor their official php api.

Contribute!
===========
I'm very happy if you open **pull requests** or **issues** to help making this API **more awesome**.

Installation
============
This library can be found on [Packagist](https://packagist.org/packages/g87andres/airnow-php-api).
The recommended way to install this is through [composer](http://getcomposer.org).

Edit your `composer.json` and add:

```json
{
    "require": {
        "g87andres/airnow-php-api": "dev-master"
    }
}
```

And install dependencies:

```bash
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install
```


Example call
============
```php
<?php
use g87andres\AirNow;
use g87andres\AirNow\Exception as AirNowException;

// Must point to composer's autoload file.
require('vendor/autoload.php');

// Get AirNow object. Don't use caching (take a look into Example_Cache.php to see how it works).
$airnow = new AirNow();

// Your API Key
$appid = '';

try {
    $forecast = $airnow->getForecast(92782, 'zipCode', null, null, $appid);
} catch(AirNowException $e) {
    echo 'AirNow exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
    echo "<br />\n";
} catch(\Exception $e) {
    echo 'General exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
    echo "<br />\n";
}

echo $forecast->aqi;
```

License
=======
MIT — Please see the [LICENSE file](https://github.com/g87andres/AirNow-PHP-Api/blob/master/LICENSE) distributed with this source code for further information regarding copyright and licensing.

**Please check out the following links to read about the usage policies and the license of AirNow before using the service.**
- [AirNow.gov](http://www.airnow.gov)